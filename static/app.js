var app = angular.module('app', [], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

app.controller('MainCtrl', ['$scope', '$http', function($scope, $http) {


$scope.hello = 'RANDOM GUESS'

$scope.showdiv = false;

$scope.submitnum = function(){
$scope.message = ''


    $http({
                url: '/getnumber',
                method: "POST",
                data: JSON.stringify({
                    'number': $scope.number_input,
                }),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(function(response) {
                if(response.data.status==true){
                    $scope.cnumber = response.data.cnumber
                    $scope.hnumber = response.data.hnumber
                    $scope.correct = response.data.correct

                    $scope.showdiv = true;
                   console.log(response)
                }
                else{
                    $scope.message = response.data.message
                }
                },
                function(response) { // optional
                    console.log(response)
                });







}

}]);
